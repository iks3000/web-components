const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

export const validate = (email) => {
    const inputEnding = email.substring(
        email.indexOf("@") + 1
    );
    console.log(VALID_EMAIL_ENDINGS.some((value) => value === inputEnding));
}

