
import { unsubscribe } from './unsubscribe';
import { subscribe } from './subscribe';
import { displayForm, displayUnsubscribe } from './display-elements';
import { getButton, joinSection, formDetails, inputEmail, unsubscribeBtn } from './query-selectors';

const emailLocalStorage = localStorage.getItem("email");

const urlUnsubscribe = '/unsubscribe';
const urlSubscribe = '/subscribe';

export const events = () => {
    window.addEventListener("load", () => {
        if (joinSection !== null) joinSection.style.display = "block";
        if (inputEmail !== null) {
            subscribe(formDetails, urlSubscribe, inputEmail, getButton);
        };
        unsubscribe(unsubscribeBtn, urlUnsubscribe);
        console.log(emailLocalStorage);
        emailLocalStorage ? displayUnsubscribe() : displayForm();
    });
};

