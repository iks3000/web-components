import { hamburger } from "./hamburger";
import { events} from "./join-us-section";
import { getUser } from './get-users';
import WebsiteSection from './web-component';

hamburger();
events();
getUser();
